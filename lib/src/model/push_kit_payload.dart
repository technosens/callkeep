class PushKitPayloadKeys {
  PushKitPayloadKeys({
    required this.handleKey,
    required this.handleTypeKey,
    required this.hasVideoKey,
    this.uuidKey,
    this.localizedCallerNameKey,
  });

  final String? uuidKey;
  final String handleKey;
  final String handleTypeKey;
  final String hasVideoKey;
  final String? localizedCallerNameKey;

  Map<String, dynamic> toJson() {
    return {
      'uuidKey': uuidKey,
      'handleKey': handleKey,
      'handleTypeKey': handleTypeKey,
      'hasVideoKey': hasVideoKey,
      'localizedCallerNameKey': localizedCallerNameKey,
    };
  }
}
