import 'package:callkeep/callkeep.dart';

class PushKitPayloadConfiguration {
  const PushKitPayloadConfiguration({
    this.payloadKeys,
    this.allowAlertType = false,
  });

  final PushKitPayloadKeys? payloadKeys;
  final bool allowAlertType;

  Map<String, dynamic> toJson() {
    return {
      'payloadKeys': payloadKeys?.toJson(),
      'allowAlertType': allowAlertType,
    };
  }
}
