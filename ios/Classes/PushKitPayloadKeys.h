#import <Foundation/Foundation.h>

@interface PushKitPayloadKeys : NSObject

@property (nonatomic, strong) NSString * _Nonnull uuidKey;
@property (nonatomic, strong) NSString * _Nonnull handleKey;
@property (nonatomic, strong) NSString * _Nonnull handleTypeKey;
@property (nonatomic, strong) NSString * _Nonnull hasVideoKey;
@property (nonatomic, strong) NSString  * _Nullable localizedCallerNameKey;

-(instancetype _Nullable ) initWithUuidKey: (NSString *_Nonnull)uuidKey handleKey: (NSString *_Nonnull) handleKey handleTypeKey: (NSString*_Nonnull) handleTypeKey hasVideoKey: (NSString*_Nonnull) hasVideoKey localizedCallerNameKey: (NSString * _Nullable) localizedCallerNameKey;

-(instancetype _Nonnull ) initWithDictionaty: (NSDictionary *_Nonnull) dict;

@end
