#import <Foundation/Foundation.h>
#import "PushKitPayloadKeys.h"

@implementation PushKitPayloadKeys : NSObject 

-(instancetype) initWithUuidKey: (NSString *_Nonnull)uuidKey handleKey: (NSString *_Nonnull) handleKey handleTypeKey: (NSString*_Nonnull) handleTypeKey hasVideoKey: (NSString*_Nonnull) hasVideoKey localizedCallerNameKey: (NSString * _Nullable) localizedCallerNameKey {
    self = [super init];
    
    if (self) {
        self.uuidKey = uuidKey;
        self.handleKey = handleKey;
        self.handleTypeKey = handleTypeKey;
        self.hasVideoKey = hasVideoKey;
        self.localizedCallerNameKey = localizedCallerNameKey;
    }
    
    return self;
}

-(instancetype) initWithDictionaty: (NSDictionary *) dict {
    self = [super self];
    
    if (self) {
        self.uuidKey = dict[@"uuidKey"];
        self.handleKey = dict[@"handleKey"];
        self.handleTypeKey = dict[@"handleTypeKey"];
        self.hasVideoKey = dict[@"hasVideoKey"];
        self.localizedCallerNameKey = dict[@"localizedCallerNameKeys"];
    }
    
    return self;
}

@end
